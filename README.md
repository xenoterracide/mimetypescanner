BUILD
===============
    
    mvn package
    
USAGE
===============

     java -jar target/mimetypescanner-0.1.0-SNAPSHOT.jar <directory>

EXAMPLE
===============
 
    java -jar target/mimetypescanner-0.1.0-SNAPSHOT.jar .
    
LICENSE
===============
Apache 2.0

COPYRIGHT
===============
Caleb Cushing <xentoerracide@gmail.com>

STATUS
===============
[![wercker status](https://app.wercker.com/status/2c8c9417fcbc53d10e3108a8d426d031/s/master "wercker status")](https://app.wercker.com/project/byKey/2c8c9417fcbc53d10e3108a8d426d031)

