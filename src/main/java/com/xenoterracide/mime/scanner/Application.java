package com.xenoterracide.mime.scanner;

import org.apache.tika.Tika;
import org.apache.tika.mime.MimeTypes;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

@SpringBootApplication
public class Application {

    public static void main( final String... args ) {
        SpringApplication.run( Application.class, args );
    }

    @Bean
    static Tika tika() {
        return new Tika();
    }

    @Bean
    static MimeTypes mimeTypes() {
        return MimeTypes.getDefaultMimeTypes();
    }

    private static <T> Function<Path, T> handleEx( final ThrowsException<Path, T> delegate ) {
        Objects.requireNonNull( delegate );
        return path -> {
            try {
                return delegate.apply( path );
            }
            catch ( Exception e ) {
                e.printStackTrace();
            }
            return null;
        };
    }

    private static String detect( final Path path ) throws IOException {
        return tika().detect( path );
    }

    @Bean
    CommandLineRunner commandLineRunner() {
        return args -> {
            Stream.of( args )
                    .map( Paths::get )
                    .filter( Files::isDirectory )
                    .flatMap( handleEx( Files::walk ) )
                    .filter( Objects::nonNull )
                    .filter( Files::isRegularFile )
                    .map( handleEx( Application::detect ) )
                    .filter( Objects::nonNull )
                    .sorted()
                    .distinct()
                    .forEach( System.out::println );
        };
    }

    @FunctionalInterface
    private interface ThrowsException<I, R> {
        R apply( I i ) throws Exception;
    }
}
